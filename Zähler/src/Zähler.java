import java.util.Scanner;
public class Z�hler {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Bitte den Startwert in Celsius eingeben: ");
        int startwert = sc.nextInt();
        System.out.println("Bitte den Endwert in Celsius eingeben: ");
        int endwert = sc.nextInt();
        System.out.println("Bitte die Schrittweite in Grad Celsius eingeben: ");
        int schrittweite = sc.nextInt();
        int zaehler = startwert;
        while(zaehler<=endwert) {
            double fahrenheit = 32+(zaehler*1.8);
            System.out.println(zaehler + " �C und " + fahrenheit + " �F");
            zaehler = zaehler+schrittweite;
        }
    }

}