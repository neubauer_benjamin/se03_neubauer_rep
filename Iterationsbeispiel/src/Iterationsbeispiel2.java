import java.util.Scanner;

public class Iterationsbeispiel2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte n ein: ");
		int n = myScanner.nextInt();

		int zaehler = n;

		while (zaehler > 0) {
			if (zaehler == 1) {
				System.out.print(zaehler);
			} else {
				System.out.print(zaehler + ", ");
			}
			zaehler--;
			myScanner.close();
		}

	}
}
