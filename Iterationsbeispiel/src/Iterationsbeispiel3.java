import java.util.Scanner;

public class Iterationsbeispiel3 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte n ein: ");
		int n = myScanner.nextInt();

		int zaehler = 1;
		int ergebnis = n * zaehler;
		while (zaehler < n) {
			ergebnis = ergebnis * zaehler;
			zaehler++;
		}
		System.out.println("Ergebnis: " + ergebnis);
		myScanner.close();
	}

}