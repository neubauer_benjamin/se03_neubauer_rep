import java.util.Scanner;

public class Iterationsbeispiel {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte n ein: ");
		int n = myScanner.nextInt();

		int zaehler = 1;

		while (zaehler <= n) {
			if (zaehler == n) {
				System.out.print(zaehler);
			} else {
				System.out.print(zaehler + ", ");
			}
			zaehler++;
			myScanner.close();
		}

	}
}
